$("#document").ready(function () {
    /*
    todo : Giải thích hiển thị cửa sổ
    Cửa sổ hiển thị thuộc tính phải được chạy đầu tiên vì cần phải tạo
    overlay để tựa cửa sổ vào trong map (#)
    # = nếu để phần hiển thị chạy trước sẽ gây ra vấn đề cửa sổ popup sẽ không thể neo vào bản đồ , gây ra lỗi
    * */
// todo : module5
    /*5. Tao cua so hien thi thuoc tinh
    module5 : nhấp vào bản đồ để có được 1 cửa sổ bật lên
    // cửa sổ bật lên gồm 3 thành phần :
    vùng chứa (popup) , nội dung (content) và vùng đóng (closer)
     */
    var container = document.getElementById("popup");
    var content = document.getElementById("popup-content");
    var closer = document.getElementById("popup-closer");
// tạo biến overlay để trỏ cửa số bật lên vào bản đồ
    var overlay = new ol.Overlay({
        element: container,
        autoPan: true,
        autoPanAnimation: {
            duration: 250,
        },
    });
// các thuộc tính view khi bật bản đồ lần đầu tiên
// rotation : xoay bản đồ
    var shouldUpdate = true;
    var center = [564429.04, 2317738.2];
    var zoom = 16.56631263565161;
    var rotation = 0;


// đóng
    closer.onclick = function () {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
    };

///Hiển thị lớp bản đồ lên web
    var format = "image/png"; // format theo dạng image/png
    var bounds = [564234.25, 2317503.0, 564510.3125, 2317932.25];  // kích thước bản đồ
    var vung = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            ratio: 1,
            url: "http://localhost:8080/geoserver/dh11c4/wms",
            params: {
                FORMAT: format,
                VERSION: "1.1.0",
                STYLES: "",
                LAYERS: "dh11c4:camhoangdc_1",
            },
        }),
    });

    var duong = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            ratio: 1,
            url: "http://localhost:8080/geoserver/dh11c4/wms",
            params: {
                FORMAT: format,
                VERSION: "1.1.0",
                STYLES: "",
                LAYERS: "dh11c4:camhoanggt_1",
            },
        }),
    });

    var diem = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            ratio: 1,
            url: "http://localhost:8080/geoserver/dh11c4/wms",
            params: {
                FORMAT: format,
                VERSION: "1.1.0",
                STYLES: "",
                LAYERS: "dh11c4:camhoangub_1",
            },
        }),
    });
// khai báo hệ tọa độ , đơn vị , kinh tuyến trục
    var projection = new ol.proj.Projection({
        code: "EPSG: 3405",
        units: "m",
        axisOrientation: "neu",
    });
    // hiển thị
    var view = new ol.View({
        projection: projection,
        center: center,
        zoom: zoom,
        rotation: rotation,
    });
    // các đối tượng trong map , các lớp layer , lớp phủ
    var map = new ol.Map({
        target: "map",
        layers: [vung, duong, diem],
        overlays: [overlay],
        view: view,
    });
// điểm chỉnh xem map phù hợp với kích thược
    // map.getSize() phạm vi toàn bộ bản đồ
    map.getView().fit(bounds, map.getSize());
// Hiển thị nổi bật đối tượng được chọn dang vùng:

    // tạo thêm lớp vector
    var styles = {
        'MultiPolygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: "red",
                width: 5,
            }),
        }),
    };

    var styleFunction = function (feature) {
        return styles[feature.getGeometry().getType()];
    };

    var vectorLayer = new ol.layer.Vector({
        style: styleFunction,
    });

    map.addLayer(vectorLayer);
//Lấy thông tin khi click chuột
    map.on("singleclick", function (evt) {
        var view = map.getView();
        var viewResolution = view.getResolution();
        var source = vung.getSource();
        var url = source.getFeatureInfoUrl(
            evt.coordinate,
            viewResolution,
            view.getProjection(),
            {INFO_FORMAT: "application/json", FEATURE_COUNT: 50}
        );
        if (url) {
            $.ajax({
                type: "POST",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (n) {
                    var content = "<table>";
                    for (var i = 0; i < n.features.length; i++) {
                        var feature = n.features[i];
                        var featureAttr = feature.properties;
                        content +=
                            // "<tr><th bgcolor=\"aqua\">Loại đất</th>" +
                            // "<th bgcolor=\"aqua\">Chủ sử dụng</th>" +
                            // "<th bgcolor=\"#8fbc8f\">Diện tích</th></tr>"+
                            // "<tr><td bgcolor=\"aqua\">"+featureAttr["txtmemo"]+"</td>" +
                            // "<td bgcolor=\"aqua\">"+featureAttr["chusd"]+"</td>" +
                            // "<td bgcolor=\"#8fbc8f\">"+featureAttr["shape_area"]+"</td>"
                            // "</tr>";
                        "<tr><td>Loại đất : " +featureAttr["txtmemo"]+
                            "<td>Chủ sở hữu : " +featureAttr["chusd"]+
                            "<td>Diện tích : " +featureAttr["shape_area"]+
                            "</td></tr>";
                    }
                    content += "</table>";
                    $("#info").html(content);
                    // 5. Sửa bài 3 thay info thành popup-content
                    $("#popup-content").html(content);
                    overlay.setPosition(evt.coordinate);

                    // 4. Hiển thị vector nổi bật đối tượng dạng vùng
                    var vectorSource = new ol.source.Vector({
                        features: new ol.format.GeoJSON().readFeatures(n),
                    });
                    vectorLayer.setSource(vectorSource);
                },
            });
        }
    });

// Tạo check box
    $("#checkvung").change(function () {
        if ($("#checkvung").is(":checked")) {
            vung.setVisible(true);
        } else {
            vung.setVisible(false);
        }
    });

    $("#checkduong").change(function () {
        if ($("#checkduong").is(":checked")) {
            duong.setVisible(true);
        } else {
            duong.setVisible(false);
        }
    });

    $("#checkdiem").change(function () {
        if ($("#checkdiem").is(":checked")) {
            diem.setVisible(true);
        } else {
            diem.setVisible(false);
        }
    });
})

// var updatePermalink = function () {
//     if (!shouldUpdate) {
//         // do not update the url when the view was changed
//         shouldUpdate = true;
//         return;
//     }
//
//     var center = view.getCenter();
//     var hash =
//         "#map=" +
//         view.getZoom() +
//         "/" +
//         Math.round(center[0] * 100) / 100 +
//         "/" +
//         Math.round(center[1] * 100) / 100 +
//         "/" +
//         view.getRotation();
//     var state = {
//         zoom: view.getZoom(),
//         center: view.getCenter(),
//         rotation: view.getRotation(),
//     };
//
//     window.history.pushState(state, "map", hash);
// };
//
// map.on("moveend", updatePermalink);
//
// window.addEventListener("popstate", function (event) {
//     if (event.state === null) {
//         return;
//     }
//
//     map.getView().setCenter(event.state.center);
//     map.getView().setZoom(event.state.zoom);
//     map.getView().setRotation(event.state.rotation);
//     shouldUpdate = false;
// });
//
// function di_den_diem(x, y) {
//     var vi_tri = ol.proj.fromLonLat([x, y], projection);
//     view.animate({
//         center: vi_tri,
//         duration: 2000,
//         zoom: 20,
//     });
// }

                //code tim kiem doi tuong
// if (window.location.hash !== "") {
//     var hash = window.location.hash.replace("#map=", "");
//     var parts = hash.split("/");
//
//     if (parts.length === 4) {
//         zoom = parseInt(parts[0], 10);
//         center = [parseFloat(parts[1]), parseFloat(parts[2])];
//         rotation = parseFloat(parts[3]);
//     }
// }
